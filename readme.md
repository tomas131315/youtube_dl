Для развертывания:
* `git clone https://gitlab.com/tomas131315/youtube_dl.git`
* создать виртуальное окружение: `python -m venv venv`
* войти в виртуальное окружение: `venv\Scripts\activate.bat` или `source venv/bin/activate`
* установить зависимости: `pip install -U -r requirements.txt`

Для запуска:
* войти в виртуальное окружение: `venv\Scripts\activate.bat` или `source venv/bin/activate`
* запустить скрипт: `python youtube.py`