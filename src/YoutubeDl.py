import sys
from datetime import datetime, timedelta
from typing import Union

from pytube import YouTube, Stream
from pytube import exceptions

from src.Hepler import StringHelper


class YoutubeDl:
    BASE_URL = 'https://www.youtube.com/watch?v='

    def __init__(self):
        self.url = None
        self.stream = None  # type: Union[Stream, None]
        self.code = None

    def run(self):
        yt = None
        while not yt:
            try:
                yt = YouTube(self.get_video_url())
            except Exception as e:
                if isinstance(e, (exceptions.RegexMatchError, exceptions.VideoUnavailable)):
                    self.url = None
                print('Error: ' + e.__str__())
        self.print_video_info(yt)
        while not self.stream:
            self.stream = self.get_stream(yt)
            if input('Download? y/n: ') != 'y':
                self.stream = None
        print('Downloading...')
        self.stream._monostate['on_progress'] = self.print_progress
        self.stream.__dict__.setdefault('time_start', datetime.today().timestamp())
        self.stream.download(filename_prefix=self.code + '_')
        print('Downloaded!')

    @staticmethod
    def print_progress(stream: Stream, chunk, file_handler, bytes_remaining):
        stream.__dict__.setdefault('old_bytes_remaining', bytes_remaining)
        filesize = stream.filesize
        bytes_downloaded = filesize - bytes_remaining

        procent_downloaded = int(bytes_downloaded * 1000 / filesize) / 10
        stream.__dict__.setdefault('procent', procent_downloaded)
        if procent_downloaded <= stream.procent:
            return
        stream.procent = procent_downloaded

        timestamp = int(datetime.today().timestamp())
        stream.__dict__.setdefault('timestamp', timestamp)
        if timestamp <= stream.timestamp:
            return
        delta = timestamp - stream.timestamp
        stream.timestamp = timestamp

        stream.__dict__.setdefault('speed', 0)
        speed = int((stream.old_bytes_remaining - bytes_remaining) / delta)
        speed = (speed + stream.speed) / 2
        stream.speed = speed

        stream.old_bytes_remaining = bytes_remaining
        est_time = int(bytes_remaining / speed)
        time = int(timestamp - stream.time_start)

        sizeof_fmt = StringHelper.sizeof_fmt
        sys.stdout.write('\r')
        sys.stdout.write(
            str(procent_downloaded) + '%, ' +
            str(timedelta(seconds=time)) + '/' + str(timedelta(seconds=est_time)) + ', ' +
            sizeof_fmt(bytes_downloaded) + '/' + sizeof_fmt(filesize) + '/-' + sizeof_fmt(bytes_remaining) + ', ' +
            sizeof_fmt(speed) + '/s, ' +
            datetime.fromtimestamp(timestamp + est_time).__format__('est_end: %d-%m-%Y %H:%M:%S')
        )
        sys.stdout.flush()

    @staticmethod
    def get_stream(yt: YouTube) -> Stream:
        from src.Hepler import StringHelper
        sizeof_fmt = StringHelper().sizeof_fmt
        print('Streams:')
        for stream in yt.fmt_streams:
            print(stream)
        stream = None  # type: Union[Stream, None]
        while not stream:
            stream = yt.streams.get_by_itag(input('Enter stream itag number:') or 0)
        print('Your selected stream:')
        print(stream)
        print('Filename: ' + stream.default_filename)
        print('Size: ' + sizeof_fmt(stream.filesize))
        return stream

    @staticmethod
    def print_video_info(yt: YouTube):
        print('Found!!!')
        print('Length: ' + str(timedelta(seconds=int(yt.length))) + ' (' + yt.length + 's)')
        print('Title: ' + yt.title)

    def get_video_url(self):
        if not self.url:
            self.url = self.BASE_URL + self.get_video_code()
        print('Get info for URL: ' + self.url)
        return self.url

    def get_video_code(self):
        self.code = input('Enter video code: ')
        return self.code
